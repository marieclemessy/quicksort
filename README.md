# README #

### What is this repository for? ###

* This repository holds a program answering the programming assignment for week 2 of the Coursera ["Algorithms: Design and Analysis, Part 1"](https://www.coursera.org/course/algo) course by Standford University professor Tim Roughgarden.
* The goal of the programming assignment is to quickly sort the given array of 10000 distinct numbers using in-place inversions. The program returns the number of inversions performed to sort the array, comparing three strategies to select a pivot for the partition (the algorithm is based on a Divide & Conquer approach).
* Detailed explanation of the programming assignment can be found [here](https://www.dropbox.com/s/48dk4ptsv3m06nj/Homework%20_%20Coursera%20-%20week%202.pdf?dl=0).

### How do I get set up? ###

* Download the file by going to the ["Source" section](https://bitbucket.org/marieclemessy/quicksort/src) of this page.
* Start your console and compiling the downloaded file (javac QuickSort.java)
* Run the program (java QuickSort)

### Who do I talk to? ###

* Repo owner or admin : Marie Clemessy